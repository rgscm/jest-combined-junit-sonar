'use strict';

const jestJunit = require('jest-junit');
const jestSonarReporter = require('jest-sonar-reporter');

module.exports = results => jestJunit(jestSonarReporter(results));
